<?php
/*
Plugin Name: Chat Plugin
Plugin URI: http://example.com
Description: Simple chat with WP login
Version: 1.0
Author: Chris Black
Author URI: https://blackcj.com
*/

function cp_html_code() {
	$validLogin = is_user_logged_in();
	if(!$validLogin) {
      cp_showLoginForm();
	} else {
		  cp_showChatWindow();
	}
}

/**
 * Generate HTML for the log in form.
 *
 */
function cp_showLoginForm() {
    echo '<form action="' . esc_url( $_SERVER['REQUEST_URI'] ) . '" method="post">';
    echo '<p>';
    echo 'Username<br />';
    echo '<input type="text" name="cp-username" pattern="[a-zA-Z0-9 ]+" value="' . ( isset($_POST["cp-username"]) ? esc_attr( $_POST["cp-username"] ) : '' ) . '" size="40" />';
    echo '</p>';
    echo '<p>';
    echo 'Password<br />';
    echo '<input type="password" name="cp-password" value="' . ( isset( $_POST["cp-password"] ) ? esc_attr( $_POST["cf-email"] ) : '' ) . '" size="40" />';
    echo '</p>';
    echo '<p><input type="submit" name="cf-submitted" value="Send"/></p>';
    echo '</form>';
}
		
/**
 * Generate HTML for the chat window.
 * TODO: This is a work in progress.
 *
 */
function cp_showChatWindow() {
    global $wpdb;
    
    $user = wp_get_current_user();
    $user_id = $user->ID;
    $user_login = $user->user_login;
    echo '<h3>Welcome ' . $user_login . "</h3>";
  
    // Store our table names in variables to make accessing them easier
    $chat_table_name = $wpdb->base_prefix . "cp_chat"; // i.e. wp_cp_chat, we add cp to ensure it's unique
    $user_table_name = $wpdb->base_prefix . "users"; // Existing users table
  
    // If a message was submitted, post it before our SELECT query
    if(isset($_POST["cp-message"])) {
      $cp_text = mysql_real_escape_string($_POST["cp-message"]);
      $insert = "INSERT INTO " . $chat_table_name .
            " (user_id, message_text) " .
            "VALUES ('$user_id','$cp_text')";
      $results = $wpdb->query( $insert );
    }


    $sql_query = "SELECT chat.entry_time, chat.message_text, cuser.user_login 
                  FROM $chat_table_name as chat
                  INNER JOIN $user_table_name as cuser ON cuser.ID = chat.user_id
                  LIMIT 0, 30;";
    $cp_chat_rows = $wpdb->get_results( $sql_query );
    foreach ( $cp_chat_rows as $cp_row ) 
    {
      echo $cp_row->entry_time . " <strong>" . $cp_row->user_login . "</strong>: " . htmlspecialchars($cp_row->message_text) . "<br />";
    }
  
    // Form to accept a new message
    echo '<form action="' . esc_url( $_SERVER['REQUEST_URI'] ) . '" method="post">';
    echo '<p>';
    echo 'Message<br />';
    echo '<input type="text" name="cp-message" pattern="[a-zA-Z0-9!@#() ]+" size="40" />';
    echo '</p>';
    echo '<p><input type="submit" name="cf-submitted" value="Send"/></p>';
    echo '</form>';

}

function cp_shortcode() {
    ob_start(); // Buffer our HTML output
	
    cp_html_code(); // Generate our HTML output

    return ob_get_clean(); // Flush the buffer to the screen
}

add_shortcode( 'chat_window', 'cp_shortcode' );

/**
 * We must execute the login code before HTML is written to the page. This can be done by
 * registering our check login function the template_redirect action. 
 */
function cp_check_login(){
	if(isset($_POST["cp-username"]) && isset($_POST["cp-password"])) {
		
    $creds = array(
        'user_login'    => $_POST["cp-username"],
        'user_password' => $_POST["cp-password"],
        'rememember'    => true
    );
 
    $user = wp_signon( $creds, false );
		if ( !is_wp_error($user) ) {
      wp_set_current_user($user->ID);
		}
	}
}

// When we redirect, check the login before rendering HTML
add_action('template_redirect', 'cp_check_login');


/**
 * Database setup. Hooks MUST be defined in this file but it's starting to get pretty big
 * so let's move our implementation to a separate file (chat-plugin-db-setup.php).
 */
require_once (dirname(__FILE__) . '/chat-plugin-db-setup.php');
register_activation_hook( __FILE__, 'cp_install' ); // Called when our plugin is activated
register_activation_hook( __FILE__, 'cp_install_data' ); // Called when our plugin is activated

/**
 * Admin page setup.
 */
function wpdocs_register_cp_custom_menu_page() {
    add_menu_page(
        __( 'Chat Plugin', 'textdomain' ),
        'Chat Options',
        'manage_options',
        'chat-plugin/admin-options.php',
        '',
        plugins_url( 'chat-plugin/images/icon.png' ),
        6
    );
}
add_action( 'admin_menu', 'wpdocs_register_cp_custom_menu_page' );
