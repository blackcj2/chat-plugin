<?php
global $wpdb;

echo "<h2>Chat Plugin</h2>";

echo "<form action='' method='POST'";
echo "<fieldset class='checkbox'>";
echo "<legend><h3>Message History</h3></legend>";

// Define our table names
$chat_table_name = $wpdb->base_prefix . "cp_chat"; // i.e. wp_cp_chat, we add cp to ensure it's unique
$user_table_name = $wpdb->base_prefix . "users"; // Existing users table

// Check for posted messages to delete
if(isset($_POST['cp_messages'])) {
  $messages_to_delete = $_POST['cp_messages'];
  // implode is a function to make generating a list of id's easier
  $sql_query = "DELETE FROM $chat_table_name WHERE id IN (".implode(",",$messages_to_delete).");";
  // Run the query to remove the selected items
  $wpdb->query($sql_query);
}

// Query the database for all messages
$sql_query = "SELECT chat.id, chat.message_text, cuser.user_login 
              FROM $chat_table_name as chat
              INNER JOIN $user_table_name as cuser ON cuser.ID = chat.user_id
              LIMIT 0, 30;";
$cp_chat_rows = $wpdb->get_results( $sql_query );

// Print each of the messages to the screen
foreach ( $cp_chat_rows as $cp_row ) 
{
  echo "<label>";
  echo "<input type='checkbox' name='cp_messages[$cp_row->id]' value='$cp_row->id' />". htmlspecialchars($cp_row->message_text) . " by $cp_row->user_login";
  echo "</label><br />";
}
echo "</fieldset>";
echo "<input type='submit' value='Delete Selected' />";
echo "</form>";