-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: May 05, 2016 at 11:43 PM
-- Server version: 5.5.38
-- PHP Version: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_chat`
--

CREATE TABLE `wp_chat` (
`id` int(11) NOT NULL,
  `message` text NOT NULL,
  `reply_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp_chat`
--

INSERT INTO `wp_chat` (`id`, `message`, `reply_id`, `user_id`, `datetime`) VALUES
(1, 'Hello world. This is a test message.', NULL, 1, '2016-05-05 21:08:06'),
(2, 'Another test message.', NULL, 1, '2016-05-05 21:08:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_chat`
--
ALTER TABLE `wp_chat`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_chat`
--
ALTER TABLE `wp_chat`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;