<?php

global $cp_db_version;
$cp_db_version = '1.0';

function cp_install_data() {
	global $wpdb;
	
	$user_id = 1;
	$message_text = 'Congratulations, you just completed the installation!';
	
	$table_name = $wpdb->prefix . 'cp_chat';
	
	$wpdb->insert( 
		$table_name, 
		array( 
			'user_id' => $user_id, 
			'message_text' => $message_text
		) 
	);
}

function cp_install() {
	global $wpdb;
	global $cp_db_version;

	$table_name = $wpdb->prefix . 'cp_chat';
	$charset_collate = $wpdb->get_charset_collate();
  $installed_ver = get_option( "cp_db_version" );

  if ( $installed_ver != $cp_db_version ) {
    $sql = "CREATE TABLE $table_name (
      id mediumint(9) NOT NULL AUTO_INCREMENT,
      user_id mediumint(9) NOT NULL,
      reply_id mediumint(9) DEFAULT NULL,
      message_text text NOT NULL,
      entry_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
      UNIQUE KEY id (id)
    );";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );

    update_option( 'cp_db_version', $cp_db_version );
  }
}